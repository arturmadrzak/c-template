#include <stdio.h>
#include "module.h"

int main() {
    printf("Hello world!\n");
    printf("5+3=%d\n", module_add(5, 3));
    printf("5-3=%d\n", module_sub(5, 3));
    return 0;
}
