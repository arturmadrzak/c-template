[![pipeline status](https://gitlab.com/arturmadrzak/c-template/badges/master/pipeline.svg)](https://gitlab.com/arturmadrzak/c-template/commits/master)

## Simple module
In project structure the simplest `module` consists 2 files:
```
module.c
tests/test_module.c
```
A module logic usable by application or library resides in `<module name>.c`. 
Test suite source file `test_<module name>.c` has to be put in `tests/` directory and can be created based on `test_main.c` template. 
The naming convention is very important and prefix `test_` has to be in place as well as `<module name>` must be the same. `make` tool utilises
this convetion to properly compile and link objects. Having these two files is enough to build and test `<module name>` which is done by invoking:
```
make
```
That is a very simple module example. To make use of module functionality in other parts of project the header file is required which can be included.
Noramlly, header file is added in the same place as source file, but in this case convention is no so strict. For more flexibility new directory 
containing ie. public headers can be created. Having given header, there is possibility to mock up modules while testing other parts of code.

## Mockups
### Create Mockup Source File
Mocukp sources follow naming convention as well and all should be placed in `tests/`. The file name for mockup is `mock_<module name>.c`. Refer `tests/mock_module.c`
to see how this can be done. By default, creation of `mock_<module name>.c` doesn't change anything and file isn't even compiled. Making it availabe for any test suite
put the file into linking list and trigger compilation process.

### Make it available for specific test suite
Mockup has to be manually added to specific test suite. To do that, add depenency rule in `tests/Makefile`. Naming convenction must be in place:
```
suite_<module name>.so: mock_<mock module name>.o
subsuite_<module name>.<suite name>.so: mock_<mock module name>.o
```
In that way, compiled mockup is added to/linked into testing suite and available to use.


## Complex module with multiple test suites
In may real world use cases module is complex and one suite for the whole module is not enough. Take into account API calls where each should be stressed in many ways.
That's the reason to have posibility to define test suite for ie. each module function(method) in separate file. The naming convention to do that is:
```
<module name>.c
test_<module_name>.<suite name>.c
```
This naming convention says to build test suite with `<module name>.o` linked with `test_<module_name>.<suite name>.o` and any addintional mockups required. Mockups are added in 
the same way as in simple module. 

## License
Copyright (c) 2020 Artur Mądrzak <artur@madrzak.eu>  
The **c-template** is available under the MIT License.

Powered by https://cgreen-devs.github.io
